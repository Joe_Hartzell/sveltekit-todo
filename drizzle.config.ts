import "dotenv/config"
import type { Config } from 'drizzle-kit';

export default {
    schema: "./src/lib/server/schema.ts",
    driver: "pg",
    dbCredentials: {
        connectionString: process.env.DATABASE_URL as string,
    },
    out: "./drizzle"
} satisfies Config;
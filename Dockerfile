# STAGE: copy the cached files
FROM node:16-alpine as base
WORKDIR /app

# libc6-compat is required for turbo see -> https://github.com/vercel/turbo/issues/2198
RUN apk add --no-cache libc6-compat && \
    npm i -g pnpm
COPY pnpm-lock.yaml .
RUN pnpm fetch 

# STAGE: build the production application
FROM base as build

ARG DATABASE_URL 
ENV DATABASE_URL=$DATABASE_URL

WORKDIR /app

# copy the rest of the src files
COPY . .
RUN pnpm i -r --offline && \
    pnpm build

# STAGE: build the deployment package
FROM base as deploy
WORKDIR /app
ENV PORT=80

# copy the build artifacts
COPY --from=build /app/build /app/build
COPY package.json .

# install production dependencies
RUN pnpm i -r --prod

CMD ["node", "/app/build"]

# install curl
RUN apk --no-cache add curl

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl", "http://0.0.0.0/healthz" ]

import { deleteTodo } from "$lib/server/services/todo.service.js";
import { json } from "@sveltejs/kit";

export async function DELETE({ params, request }) {
    const userId = request.headers.get("x-user-id") as string;
    const id = parseInt(params.id);

    await deleteTodo(userId, id);

    return json(null, { status: 200 });
}
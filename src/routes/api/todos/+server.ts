import type { NewTodo } from "$lib/server/schema";
import { json } from "@sveltejs/kit";
import { createTodo, getTodos } from "$lib/server/services/todo.service";

function getPaginationOptions(url: URL) {
  const page = url.searchParams.get("page");
  const limit = url.searchParams.get("limit");

  return {
    limit: limit ? parseInt(limit) : 10,
    page: page ? parseInt(page) : 0,
  };
}

export async function POST({ request }) {
  const data: Omit<NewTodo, 'user'> = await request.json();
  const userId = request.headers.get("x-user-id") as string;

  const { id } = await createTodo({ ...data, user: userId });

  return json({ id }, { status: 200 });
}

export async function GET({ request, url }) {
  const userId = request.headers.get("x-user-id") as string;
  const { limit, page } = getPaginationOptions(url);
  const response = await getTodos(userId, page, limit);

  return json(response, { status: 200 });
}

import type { GetTodoResponse } from "$lib/type";
import { and, eq, sql } from "drizzle-orm";
import { db } from "$lib/server";
import { todos, type NewTodo, type Todo } from "$lib/server/schema";
import NodeCache from 'node-cache'

// create a cache with a 1 hour TTL
const cache = new NodeCache({ stdTTL: 60 * 60 });

async function getOrCreateCache<T>(key: string, factory: () => Promise<T>): Promise<T> {
  const value = cache.get<T>(key);
  if (value) {
    return (value);
  }

  // handle the cache miss
  const data = await factory();
  cache.set(key, data);

  // return the data
  return data;
}

/**
 * Get or create the cached todo count for the given user 
 * @param userId 
 * @returns 
 */
const getOrCreateTodoCount = async (userId: string) => await getOrCreateCache(`todo:count:${userId}`, async () => {
  const [{ count }] = await db
    .select({ count: sql<number>`COUNT(*)::int` })
    .from(todos)
    .where(eq(todos.user, userId));
  return count;
});

/**
 * Revalidate the cached todo count for the given user 
 * @param userId 
 * @returns 
 */
const revalidateTodoCount = async (userId: string) => {
  cache.del(`todo:count:${userId}`);
  await getOrCreateTodoCount(userId);
}

/**
 * Update the cached todo count for the given user 
 * @param userId 
 * @param updater 
 */
const updateTodoCount = async (userId: string, updater: (count: number) => number) => {
  const count = await getOrCreateTodoCount(userId);
  const newCount = updater(count);
  cache.set(`todo:count:${userId}`, newCount);
}

export async function getTodos(
  userId: string,
  page: number = 0,
  limit: number = 10
): Promise<GetTodoResponse> {
  const offset = page * limit;

  // get the total count of todos for this user
  const count = await getOrCreateTodoCount(userId);

  // get the todos for this user
  // paginate by the limit and offset provided by the caller
  const data = await db
    .select()
    .from(todos)
    .where(eq(todos.user, userId))
    .orderBy(todos.id)
    .limit(limit)
    .offset(offset);

  return {
    pageInfo: {
      page,
      limit,
      size: count,
    },
    items: data,
  };
}

export async function createTodo(
  data: NewTodo,
): Promise<{ id: Todo['id'] }> {
  try {
    // optimistic update the cache for this user
    await updateTodoCount(data.user, (count) => count + 1);

    const [{ id }] = await db
      .insert(todos)
      .values(data)
      .returning({ id: todos.id });

    return { id };
  } catch (error) {
    // revalidate the cache on failure
    await revalidateTodoCount(data.user);
  
    throw error;
  }
}

export async function deleteTodo(
  userId: string,
  todoId: Todo['id'],
): Promise<void> {
  try {
    // optimistic update the cache for this user
    await updateTodoCount(userId, (count) => count - 1);

    await db
      .delete(todos)
      .where(and(eq(todos.id, todoId), eq(todos.user, userId)))
  } catch (error) {
    // revalidate the cache on failure
    await revalidateTodoCount(userId);

    throw error;
  }
}
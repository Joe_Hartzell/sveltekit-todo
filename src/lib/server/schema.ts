import { pgTable, serial, text, varchar, uuid, index } from "drizzle-orm/pg-core"

export const todos = pgTable("todos", {
    id: serial('id').primaryKey(),
    user: uuid('user').notNull(), 
    title: varchar('title').notNull(),
    content: varchar('content').notNull(),
}, (table) => {
    return {
        userIdx: index("user_idx").on(table.user),
    }
})

export type Todo = typeof todos.$inferSelect;
export type NewTodo = typeof todos.$inferInsert;
import { DATABASE_URL } from '$env/static/private';
import postgres from 'postgres';
import { drizzle } from 'drizzle-orm/postgres-js';

const client = postgres(DATABASE_URL);
export const db = drizzle(client);

import type { Todo } from "$lib/server/schema";

export interface PageInfo {
  page: number;
  size: number;
  limit: number;
}

export interface GetTodoResponse {
  pageInfo: PageInfo;
  items: Todo[];
}

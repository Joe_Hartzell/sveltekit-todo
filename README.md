# Sveltekit Todo

Simple Todo application that uses Sveltekit. This application was built over the course of ~5 hours. It is simply a demonstration of how to use Sveltekit. The application supports basic CRUD functions on Todos. Each user is assigned a unique ID when visiting the site to allow all users to have their own list. 

The application is hosted in US East Coast data centers, so don't expect great performance elsewhere.

## Stack
- Database: Supabase Database (postgresql)
- Hosting: Digital Ocean Docker App ([Live Link](https://whale-app-5h5vf.ondigitalocean.app/))
- Frontend: Sveltekit
- Backend: Sveltekit Server Routes

### Libraries
- [svelte-feather-icons](https://www.npmjs.com/package/svelte-feather-icons)
- [drizzle](https://orm.drizzle.team/)
- [skeleton ui](https://www.skeleton.dev/)
- [tailwind](https://tailwindcss.com/)

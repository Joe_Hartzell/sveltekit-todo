CREATE TABLE IF NOT EXISTS "todos" (
	"id" serial PRIMARY KEY NOT NULL,
	"user" uuid NOT NULL,
	"title" varchar NOT NULL,
	"content" varchar NOT NULL
);
